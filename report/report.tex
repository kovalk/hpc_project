\documentclass[a4paper,11pt]{article}

\usepackage{geometry}
\usepackage{listings}
\usepackage{mathrsfs}
\usepackage[font={small},labelfont={sf,bf}]{caption}
\usepackage{color}
\usepackage{amssymb}
\usepackage[utf8]{inputenc}
\usepackage{afterpage}
\usepackage[T1]{fontenc}
\usepackage{amsfonts}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{verbatim}
\usepackage{bm}
\usepackage{bbm}
\usepackage{enumerate}
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{tabu}

\geometry{a4paper,top=3cm,bottom=3cm,left=2cm,right=2cm,heightrounded, bindingoffset=5mm}

\newcommand*\mcup{\mathbin{\mathpalette\mcupinn\relax}}
\newcommand*\mcupinn[2]{\vcenter{\hbox{$\mathsurround=0pt
  \ifx\displaystyle#1\textstyle\else#1\fi\bigcup$}}}
\newcommand*\mcap{\mathbin{\mathpalette\mcapinn\relax}}
\newcommand*\mcapinn[2]{\vcenter{\hbox{$\mathsurround=0pt
  \ifx\displaystyle#1\textstyle\else#1\fi\bigcap$}}}
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclarePairedDelimiter{\norm}{\lVert}{\rVert}
\DeclarePairedDelimiter{\parr}{(}{)}
\DeclarePairedDelimiter{\parq}{[}{]}
\DeclarePairedDelimiter{\parqq}{\llbracket}{\rrbracket}
\DeclarePairedDelimiter{\bra}{\lbrace}{\rbrace}

\begin{document}
\date{}
\title{Finite Volume Solution of a 2-D Wave Equation on an Adaptive Mesh Using \texttt{p4est} Library}
\author{Azam Asl, Karina Koval, Luca Venturi}
\maketitle
\section*{Problem Introduction}
\paragraph*{}
Adaptive meshes are often desirable for efficient high resolution simulations of physical phenomena. Ideally, one would like to dynamically change the spatial mesh used for solutions of partial differential equations to obtain higher accuracy, i.e., one would like a fine mesh in locations of high turbulence or large spatial gradient, and a more coarse mesh in locations where higher precision is not required. We used \texttt{p4est}, a parallel adaptive mesh refinement (AMR) library to solve a wave equation with periodic boundary conditions on a unit square.  

\section*{Introduction to \texttt{p4est} Library}
\paragraph*{}
\texttt{p4est} is a library for parallel adaptive mesh refinement. It starts with a conforming 2d/3d mesh and renders each cell as an adaptive quadtree/octree. The adaptive structure allows for quadrants/octants of different sizes to neighbor each other. Such a structure is commonly called non-conforming and leads to dealing with hanging faces and edges.
\paragraph*{}
\texttt{p4est} starts by creating a coarse mesh in the beginning. The cells of this mesh are considered to be the roots of the quadtree/octrees, which are going to be distributed on different processors. The creation of the connectivity needs to be performed redundantly on all processors. Once it is done, \texttt{p4est} modifies quadtree/octrees in place on each processors.  All operations are MPI collective and therefore, they are called simultaneously on all processors.
\paragraph*{}
The user interacts with the adaptive mesh via four main functions, which are receptively responsible for \emph{refinement}, \emph{coarsening}, \emph{balancing} and \emph{partitioning} of the mesh. The functions \textbf{refine} and  \textbf{coarsen} are controlled by callback to user defined  functions. The \textbf{balance} function ensures 2:1 size balance between neighbors. The function \textbf{partition} redistributes quadtree/octrees between processors to ensure an even workload.
\paragraph*{}
For more information on \texttt{p4est}, we refer to \cite{ghattas:p4est}.

%\cite{p4est_howto}
%The C struct p must only be changed by these API calls and is understood as read-only otherwise.
 %This could result in a distributed non-conforming mesh.   User can opt for static or dynamic load balancing. In static load balancing the initial partitioning of the nodes stays unchanged. 

\section*{Wave Equation in 2D}
\paragraph*{}
We want to solve a 2d wave equation with periodic boundary condition on the unit square. Namely we want to find $v = v(\mathbf{x},t)$, for $\mathbf{x}=(x,y)\in\Omega = [0,1]^2$, $t\geq0$ such that
$$
v_{tt} - \Delta_{\mathbf{x}} v = 0,
$$
with initial conditions 
$$
v(\mathbf{x},0) = f_0(\mathbf{x}), \qquad v_t(\mathbf{x},0) = g_0(\mathbf{x}),
$$
and periodic boundary conditions
$$
v(0,y,t) = v(1,y,t), \qquad v(x,0,t) = v(x,1,t). 
$$
\paragraph*{}
To be able to use use a finite volume method, we write the above equation as a system of first order equations. Let $\mathbf{u}(\mathbf{x},t) = (u_1(\mathbf{x},t),u_2(\mathbf{x},t),u_3(\mathbf{x},t))^T = (v_t(\mathbf{x},t),v_x(\mathbf{x},t),v_y(\mathbf{x},t))^T$. The system we want to solve is: 
\begin{equation}\label{wave_sys}
\begin{cases}
\mathbf{u}_t + A_1\mathbf{u}_x + A_2\mathbf{u}_y = 0 \\
\mathbf{u}(\mathbf{x},0) = (g_0(\mathbf{x}),\nabla_{\mathbf{x}}f_0(\mathbf{x}))^T \\
\mathbf{u}(0,y,t) = \mathbf{u}(1,y,t) \\
\mathbf{u}(x,0,t) = \mathbf{u}(x,1,t)
\end{cases}
\end{equation}
where $A_1 = \begin{bmatrix} 0 & -1 & 0 \\ -1 & 0 & 0 \\ 0 & 0 & 0 \end{bmatrix}$ and $A_2 = \begin{bmatrix} 0 & 0 & -1 \\ 0 & 0 & 0 \\ -1 & 0 & 0 \end{bmatrix}$. 

\section*{Approximation to solution using finite volume method}
\paragraph*{}
We use a finite volume method \cite{leveque:finitevolume} to solve the system (\ref{wave_sys}). We consider a partition of our domain into a finite number of square cells $V_{ij}$, i.e. $\Omega = [0,1]^2 = \bigcup_{i,j}V_{ij}$. In our discretization we assume $\mathbf{u}$ is piece-wise constant on each $V_{ij}$. We wish to obtain an approximation of $\mathbf{u}$ at discretized times $0=t_0\leq\dots\leq t_{N_T}=T$ (in our experiments we chose $T=3$). Note that in our case, due to the adaptive structure, the time-step $dt = t_{n+1} - t_n$ is not constant. We denote $\mathbf{u}_{ij}^n = \mathbf{u}|_{\mathbf{x}\in V_{ij}, t = t_n}$. \\
Integrating the equation (\ref{wave_sys}) over every discretization volume, we get
$$
0 = \int_{V_{ij}} (\mathbf{u}_t + \nabla_\mathbf{x} \cdot (A_1\mathbf{u},A_2\mathbf{u}))\,dV = \int_{V_{ij}} u_t\,dV + \int_{\partial V_{ij}} (A_1\mathbf{u},A_2\mathbf{u} )\cdot\mathbf{n}\,dS 
$$
\paragraph*{}
Given the eigenvalue decompositions of the matrices $A_i = U_i\Lambda_i U_i^{-1}$, we denote $A^{\pm}_i = U_i \Lambda_i^{\pm} U_i^{-1}$, where $\Lambda_i^{+}$ ($\Lambda_i^{-}$) is the diagonal matrix with the positive (negative) eigenvalues of $A_i$. 
If 
\begin{align*}
\Delta \mathbf{u}^n_{i-\frac{1}{2},j} & = \mathbf{u}^n_{ij}-\mathbf{u}^n_{i-1,j}, \\
\Delta \mathbf{u}^n_{i+\frac{1}{2},j} & = \mathbf{u}^n_{i+1,j}-\mathbf{u}^n_{ij},
\end{align*}
then our finite volume scheme is defined as 
$$
\mathbf{u}^{n+1}_{ij} = \mathbf{u}^n_{ij} - dt\cdot\mathbf{Q}_{ij}^n
$$ 
where $\mathbf{Q}^n_{ij} = [Q^n_{1,ij},Q^n_{2,ij},Q^n_{3,ij}]^T $ denotes the \emph{flux} at cell $V_{ij}$: 
$$
\mathbf{Q}^n_{ij} = \frac{1}{|S_{ij}|}(A_1^+\Delta{\mathbf{u}^n_{i-\frac{1}{2},j}}+A_1^-\Delta{\mathbf{u}^n_{i+\frac{1}{2},j}})
 + \frac{1}{|S_{ij}|}(A_2^+\Delta{\mathbf{u}^n_{i,j-\frac{1}{2}}}+A_2^-\Delta{\mathbf{u}^n_{i,j+\frac{1}{2}}}),
$$
where $|S_{ij}|$ is the length of one side of $V_{ij}$. Multiplying out the matrices, we obtain the following equations for the three components of the flux: 
\begin{multline*}
Q^n_{1,ij} = \frac{1}{|S_{ij}|}\big[0.5\cdot(u^n_{1,ij}-u^n_{1,i-1,j})-0.5\cdot(u^n_{2,ij}-u^n_{2,i-1,j})-0.5\cdot(u^n_{1,i+1,j}-u^n_{1,ij})-0.5\cdot(u^n_{2,i+1,j}-u^n_{2,ij}) \\ + 0.5\cdot(u^n_{1,ij}-u^n_{1,i,j-1})-0.5\cdot(u^n_{3,ij}-u^n_{3,i,j-1})-0.5\cdot(u^n_{1,i,j+1}-u^n_{1,ij})-0.5\cdot(u^n_{3,i,j+1}-u^n_{3,ij})\big],
\end{multline*}
\begin{multline*}
Q^n_{2,ij} = \frac{1}{|S_{ij}|}\big[0.5\cdot(u^n_{2,ij}-u^n_{2,i-1,j})-0.5\cdot(u^n_{1,ij}-u^n_{1,i-1,j})-0.5\cdot(u^n_{1,i+1,j}-u^n_{1,ij})-0.5\cdot(u^n_{2,i+1,j}-u^n_{2,ij})\big],
\end{multline*}
\begin{multline*}
Q^n_{3,ij} = \frac{1}{|S_{ij}|}\big[-0.5\cdot(u^n_{1,ij}-u^n_{1,i,j-1})+0.5\cdot(u^n_{3,ij}-u^n_{3,i,j-1})-0.5\cdot(u^n_{1,i,j+1}-u^n_{1,ij})-0.5\cdot(u^n_{3,i,j+1}-u^n_{3,ij})\big].
\end{multline*}

\section*{\texttt{p4est} implementation}
\paragraph*{}
We used \texttt{p4est\_step3} code as a basis for our implementation. Our main task has been to adapt the flux function in the code. In \texttt{p4est\_step3},  the flux is computed by looping over each side. For each side, we compute the contribution to the flux for each adjacent cell depending on the type of the side. The sides are as labeled as $x$ (horizontal sides) or $y$ (vertical sides). To take into account the fact that we are considering a mesh with periodic boundary, a sign is also assigned to the type of boundary: $+$ for sides which are not part of the boundary, $-$ for sides which are part of the boundary (sides of the boundary are counted only once, i.e. our domain is considered as a torus by the algorithm). We thus assign two labels $(r,s)$ to every side: $r=0$ for '$x$' sides and $r=1$ for '$y$' sides; $s=0$ for '$+$' sides and $s=1$ for '$-$' sides. Thus, given a side with label $(r,s)$, the flux of the two adjacent cells $S_0, S_1$ is updated (starting from $0$) as: 
$$
\mathbf{Q}^n_{S_i} = \mathbf{Q}^n_{S_i} + \frac{1}{2\abs{S}}\mathbf{q}_{S_i},
$$
where $\mathbf{q}_{S_i} = [q_{1,S_i},q_{2,S_i},q_{3,S_i}]^T$ is given by
$$
\begin{cases}
q_{1,S_i} = u^n_{1,S_i} + (-1)^{i+s}u^n_{2+r,S_i} - u^n_{1,S_{1-i}} + (-1)^{i+s+1}u^n_{2+r,S_{1-i}}, \\
q_{2+r,S_i} = (-1)^{i+s}q_{1,S_i}, \\
q_{3-r,S_i} = 0, 
\end{cases}
$$
for $i = 0,1$. The cells $S_0$, $S_1$ for a side label $(r,s)$ are given by Table \ref{table:S_i}. 
\begin{table}[t]
\begin{center}
\tabulinesep=1.5mm
\begin{tabu}{|c|c|c|c|c|} \hline
& $+x$ & $-x$ & $+y$ & $-y$ \\ \hline
$S_0$ & $V_{ij}$ & $V_{i+1,j}$ & $V_{ij}$ & $V_{i,j+1}$ \\ \hline
$S_1$ & $V_{i+1,j}$ & $V_{ij}$ & $V_{i,j+1}$ & $V_{ij}$ \\ \hline
\end{tabu}
\end{center}\caption{\label{table:S_i}}
\end{table}
\paragraph*{}
Several other problems had to be addressed to guarantee good performance of the algorithm and a reliable outcome.
On hanging sides the flux is updated by the average of the two fluxes resultant from the two adjacent cells.   
Many parameters had to be adjusted to guarantee the convergence of the algorithm to the real solution. In particular, the time-step needs to be changed as the minimum size of the cells in the mesh changes, so that the algorithm satisfies the CFL condition.
Finally, an additional error comes from the refining and coarsening operations
and it is not always clear what criteria is optimal to use. We decided to base the mesh refinement on approximations of the spatial derivatives of $u_1$, i.e. we refined where $|u_{1,x}|$ or $|u_{1,y}|$ were greater than some user-defined tolerance (coarsened otherwise). Different parameters had to be tuned in order to avoid a big additional error from refining and coarsening operations.

\section*{Results and Performance}
\paragraph*{}
We obtained several movies of the solution for diverse initial conditions and refining periods. The solutions we got seemed to be reliable.  
\paragraph{}
We ran the algorithm on several processors and looked at the scaling properties. For the strong scaling study, we started with $N = 65536$ cells and ran the code for 500 time steps. As we can see from Figure \ref{figure:ss}, the algorithm shows good strong scaling. On the other side, since the mesh is dynamically changing, it is not so clear how to ensure that the number of work per processor stays constant as the number of processors increases, thus we did not do a weak scaling study.
\begin{figure}[t]
\centering
\includegraphics[scale=0.4]{strong.pdf}
\caption{Strong scaling plot for starting $N = 65536$ and $500$ time-steps \label{figure:ss}.}
\end{figure}

\section*{Further directions}
\paragraph*{}
We started working on solving the 3d wave equation using \texttt{p4est} for adaptive mesh refinement. In the future it would be nice to get that code working so we could visualize the solution in 3d. Additionally, it seems that the refining and coarsening criteria may need some more fine-tuning, as currently the coarsening seems to be too aggressive in areas where it shouldn't be, leading to errors.

\bibliographystyle{plain}
\bibliography{my_biblio}

\end{document}
