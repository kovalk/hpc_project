\documentclass{beamer}

\usepackage{listings}
\usepackage{subfigure}
\usepackage{fancyhdr}
\usepackage{mathrsfs}
\usepackage{color}
\usepackage[english]{babel}
\usepackage{amssymb}
\usepackage{cite}
\usepackage{dsfont}
\usepackage{bm}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{amsthm}
\usepackage{color}
\usepackage{amssymb}
\usepackage{bbm}
\usepackage[latin1]{inputenc}
\usepackage{afterpage}
\usepackage[T1]{fontenc}
\usepackage{amsfonts}
\usepackage{mathtools}
\usepackage{verbatim}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{times}
\usepackage[T1]{fontenc}

\mode<presentation>
{
  \usetheme{Madrid}
  \setbeamercovered{invisible}
  \usefonttheme{professionalfonts}
  \usecolortheme{sidebartab}
}

\title[]{Adaptive finite volume solution of the wave equation} 

\author{Karina Koval, Luca Venturi, Azam Asl}

\institute[CIMS]
{Courant Insititute of Mathematical Sciences}

\date[05/11/2017]
{05/11/2017}

\begin{document}

\begin{frame}
  \titlepage
\centering 
{\small {High Performance Computing: Final Project}}
\end{frame}
\begin{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\frametitle{Outline}
\begin{itemize}
\item Project overview.
\item Introduction to the parallel library we used.
\item Formal Problem statement.
\item Implementation Results.
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Project overview}
We used a finite volume method to solve a 2-D wave equation on a parallel adaptive mesh.  
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{p4est Library}
\begin{itemize}
\item A library for parallel adaptive mesh refinement (AMR).
\item It uses Quadtree and Octree DS.
\item Leaves are the computational cells.
\item User's mesh is represented as one or more quadrilateral/hexahedra in the beginning. 
\item They get individually rendered as adaptive quadtrees/octrees. Results in a distributed non-conforming mesh.
\item All operations are MPI collectives.
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{General scheme to use p4est}
\begin{itemize}
\item Create a coarse mesh (almost immutable): connectivity. This can be up to $10^5-10^6$ coarse cells (octree roots).
\item Modify the refinement structure and partition it.
\item Relate the mesh information to an application.
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{How p4est helps user?}
Creating the coarse mesh in the beginning: p4est takes care of the tope row. User is involved in the bottom row.
\begin{figure}[H]
\centering
\includegraphics[width=10cm, height=3cm]{init}
\label{fig:init}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{How p4est helps user?}
Refining/coarsening the mesh in each time step:p4est takes care of the tope row. User is involved in the bottom row.
\begin{figure}[H]
\centering
\includegraphics[width=10cm, height=4cm]{bc}
\label{fig:bc}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Wave equation}
\begin{itemize}
\item We considered the equation for $v=v(t,x,y)$, $(x,y)\in[0,1]^2$, $t\geq0$,
$$
v_{tt} - \Delta v= 0
$$
with initial conditions $v(0,x,t) = 0$, $v_t(0,x,t) = \textrm{bump function}$ and periodic boundary conditions.
\vspace{3mm}
\item It can be reduced to a first order linear system. If $\mathbf{u}=(u_1,u_2,u_3)=(v_t,v_x, v_y)$, then it must satisfy
$$
\mathbf{u}_t + A_1\mathbf{u}_x + A_2\mathbf{u}_y = \mathbf{0},
$$
where
$$
A_1 = \left(\begin{matrix}
0 & -1 & 0 \\ -1 & 0 & 0 \\ 0 & 0 & 0
\end{matrix}\right), \qquad A_2 = \left(\begin{matrix}
0 & 0 & -1 \\ 0 & 0 & 0 \\ -1 & 0 & 0
\end{matrix}\right)
$$
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Finite volume method}
\begin{itemize}
\item We assume $\mathbf{u}$ constant on every cell $V_{ij}$:
$$
\mathbf{u}|_{V_{ij}} \simeq \mathbf{u}_{ij} \simeq \frac{1}{|V_{ij}|}\int_{V_{ij}} \mathbf{u}\,dx.
$$
\item Discretized version of the equation
$$
\mathbf{u}_{ij}^{n+1} = \mathbf{u}_{ij}^n + dt\cdot Q_{ij}^n
$$ 
where $Q_{ij}^n$ is the '\emph{flux}'.
\item In our case 
\begin{align*}
Q_{ij}^n = & - \frac{dt}{|Si|}(A_1^+\Delta{u^n_{i-\frac{1}{2},j}}+A_1^-\Delta{u^n_{i+\frac{1}{2},j}}) \\ & - \frac{dt}{|Sj|}(A_2^+\Delta{u^n_{i,j-\frac{1}{2}}}+A_2^-\Delta{u^n_{i,j+\frac{1}{2}}})
\vspace{5mm}
\end{align*}
\item Advantage: Finite volume method is conservative by nature, finite difference is not! 

\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Implementation}
\begin{itemize}
\item We modified the file $\texttt{p4est\_step3.c}$, in order to make it solve the wave equation above.
\vspace{5mm}
\item This accounted in changing the flux function and in adjusting many parameters.
\vspace{5mm}
\item Due to the complication added by adaptive, non-uniform mesh, compute flux by looping over faces
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Difficulties with implementation}
\begin{itemize}
\item CFL!
\vspace{5mm}
\item Additional error from refine/coarsening
\vspace{5mm}
\item Adjust parameters in order to get convergence
\vspace{5mm}
\item Not always clear what criteria is optimal to use for refinement/coarsening
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Scaling}

%\includegraphics[scale=0.5]{strong.pdf}
\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{strong}
\label{fig:strong}
\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{comment}
\begin{frame}
\frametitle{Bibliography}
\nocite{*}
\bibliography{my_biblio}
\bibliographystyle{plain}
\end{frame}
\end{comment}
\end{document}