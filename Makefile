#CC=gcc
CC=mpicc 

CFLAGS=-O0 -g -Wall -Wuninitialized
#CFLAGS="-O0 -g -Wall -Wuninitialized" \
 #                    --enable-debug --enable-mpi --disable-shared

INCLUDES=-I/usr/local/include -I./p4est/local/include
LIBS=-L./p4est/local/lib
LDFLAGS=-lp4est -lsc -lm -lz


EXECS=step3 waveFM

MAIN =waveFM.c
MAIN3D=waveFM3D.c
all: ${EXECS}

waveFM: $(MAIN)
	${CC} ${CFLAGS} $(INCLUDES) $(MAIN) $(LIBS) $(LDFLAGS) -o waveFM

waveFM3D: $(MAIN3D)
	${CC} ${CFLAGS} $(INCLUDES) $(MAIN) $(LIBS) $(LDFLAGS) -o waveFM3D

step3: step3.tex
	latexmk -pdf $^

clean:
	latexmk -pdf -c step3.tex
	rm -f waveFM
	rm -f waveFM3D

clean_paraview:
	rm -f *.vtu
	rm -f *.visit
	rm -f *.pvtu


