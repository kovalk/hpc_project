# README #

### What is this repository for? ###

HPC project repo - Adaptive finite volume solution of the wave equation using p4est

### How do I get set up? ###

git submodule init
git submodule update

Then within p4est directory, again:
git submodule init
git submodule update
Then just follow the installation instructions for p4est provided in their readme

Then run make in head directory. 

** Note! ** Currently only works properly for 4^k processors. Also waveFM3D doesn't produce expected results. 