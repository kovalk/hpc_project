%% (Hopefully?) solves : v_tt=v_xx+v_yy on [0,1]X[0,1]
%  u = [v_t,v_x,v_y], 

clear all; 
close all;
clc; 

sigma = 0.2; 
fx = @(x,y) -1*(x-1)*exp(-1/2*((x-1/2)^2+(y-1/2)^2)); % Corresponds to u_x(t=0)
v = [-1,-1];
g = @(x,y) exp(-1/8*((x-1/2)^2+(y-1/2)^2)); % Corresponds to u_t(t=0)
fy = @(x,y) -1*(y-1/2)*exp(-1*((x-1/2)^2+(y-1/2)^2)); % Corresponds to u_y(t=0)
N= 50; 
u1 = zeros(N,N);  %u_x(xi+Si/2,yj+Si/2,t)
u2 = zeros(N,N);  %u_y(xi+Si/2,yj+Si/2,t)
u3 = zeros(N,N);  %u_t(xi+Si/2,yj+Si/2,t)
% N+2 to account for boundary... ghost values, all set to zero
Si = 1./N; % |Sij|, the same for each of the squares
Vi = 1./(N*N); % |Vij| = |Sij|^2
t1 = 0; % Initial time
t2 = 3; % Final time
dt = 0.005; 
T = (t2-t1)/dt; % Num time steps
X = zeros(N,N); 
Y = zeros(N,N);
X(:,1) = 0.5*Si; 
X(:,N) = 1.-0.5*Si; 
Y(1,:) = 0.5*Si; 
Y(N,:) = 1.-0.5*Si;
A1 = [0 -1 0; -1 0 0; 0 0 0];
A2 = [0 0 -1; 0 0 0; -1 0 0];
for i = 2:N-1
    X(:,i) = i*Si+0.5*Si;
    Y(i,:) = i*Si+0.5*Si; 
end
% Set up initial values, i.e., [u1,u2,u3](t=0) = [g,fx,fy], and ensure
% ghost values are zero to ensure boundary values are zero
% NOTE: using midpoint values of each cell as the x,y coordinates
for i = 1:N
    for j = 1:N
        u2(i,j) = fx((i-2)*Si+0.5*Si,(j-2)*Si+0.5*Si);
        u3(i,j) = fy((i-2)*Si+0.5*Si,(j-2)*Si+0.5*Si);
        u1(i,j) = g((i-2)*Si+0.5*Si,(j-2)*Si+0.5*Si);
    end
end
u1(:,N) = u1(:,1);
u2(:,N) = u2(:,1); 
u3(:,N) = u3(:,N); 
u1(N,:) = u1(1,:);
u2(N,:) = u2(1,:);
u3(N,:) = u3(1,:);
%Ft(T) = struct('cdata',[],'colormap',[]);
Fx(T) = struct('cdata',[],'colormap',[]); 
Fy(T) = struct('cdata',[],'colormap',[]); 
v = VideoWriter('wave_test4.avi');
open(v); 
pcolor(u1);
surfc(X,Y,u1,'EdgeColor','none');
%set(gca,'nextplot','replacechildren');
Ft = getframe(gcf); 
writeVideo(v,Ft);

[vx1,dx1] = eig(A1); % Flux going from cell Vij from Vi+1j
dx1p = zeros(3,3);
dx1n = zeros(3,3); % Store negative eigs
for k = 1:3
    if dx1(k,k) > 0
        dx1p(:,k) = dx1(:,k); 
    end
    if dx1(k,k) < 0
       dx1n(:,k) = dx1(:,k); 
    end
end
[vy1,dy1] = eig(A2); % Flux going from cell Vij from Vij+1
dy1p = zeros(3,3);
dy1n = zeros(3,3); 
for k = 1:3
    if dy1(k,k) > 0
       dy1p(:,k) = dy1(:,k);
    end
    if dy1(k,k) < 0
       dy1n(:,k) = dy1(:,k);
    end
            
end  

%surf(X,Y,u1);
%drawnow;
%Fx(1) = getframe(gcf); 
%surf(X,Y,u2);
%drawnow;
%Fy(1) = getframe(gcf); 
for t = 1:T-1
    for i = 1:N
        for j = 1:N
        Qij = 0;
        prevx = i-1; prevy = j-1;
        nextx = i+1; nexty = j+1;
        if i == 1
            prevx = N;
        end
        if i == N
            nextx = 1;
        end
        if j == 1
            prevy = N;
        end
        if j == N
            nexty = 1; 
        end
        Qij = -1*(dt/Si)*(vx1*dx1p*inv(vx1)*([u1(i,j),u2(i,j),u3(i,j)]'-[u1(prevx,j),u2(prevx,j),u3(prevx,j)]'));
        Qij = Qij-(dt/Si)*(vx1*dx1n*inv(vx1)*([u1(nextx,j),u2(nextx,j),u3(nextx,j)]'-[u1(i,j),u2(i,j),u3(i,j)]'));
        Qij = Qij-(dt/Si)*(vy1*dy1n*inv(vy1)*([u1(i,nexty),u2(i,nexty),u3(i,nexty)]'-[u1(i,j),u2(i,j),u3(i,j)]'));
        Qij = Qij-(dt/Si)*(vy1*dy1p*inv(vy1)*([u1(i,j),u2(i,j),u3(i,j)]'-[u1(i,prevy),u2(i,prevy),u3(i,prevy)]'));
        u1(i,j) = u1(i,j)+Qij(1,1); 
        u2(i,j) = u2(i,j)+Qij(2,1); 
        u3(i,j) = u3(i,j)+Qij(3,1); 
        end
    end
    surfc(X,Y,u1,'EdgeColor','none');
    %pcolor(u1);
    Ft = getframe(gcf);
    writeVideo(v,Ft);
    
end
close(v);
