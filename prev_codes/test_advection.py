from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from mpl_toolkits.mplot3d import Axes3D

N = 50

u = np.zeros((N,N))
ut = np.zeros((N,N)) 
Q = np.zeros((N,N))

t0 = 0.
t1 = 20.
dt = 0.005
steps = int( (t1 - t0) / dt ) 
S = 1 / N
V = 1 / (N * N)

X = np.zeros((N))
Y = np.zeros((N))
for i in range(N):
	X[i] = 0.5 * S + i * S
	Y[i] = 0.5 * S + i * S
X,Y = np.meshgrid(X,Y)
C = np.array([0.5,0.5])
v = np.array([1.0,1.0])

def compute_and_plot_solution(step):

	ax.cla()

	if step == 0:
		# plot initial solution
		for i in range(N):
			for j in range(N):
				u[i,j] = np.exp(-(X[i,j]-C[0])**2-(Y[i,j]-C[1])**2)
		surf = ax.plot_surface(X,Y,u)
	else:
		# compute / plot solution at step > 0
		for i in range(N):
			for j in range(N):
				Q[i,j] = S * (u[(i+1)%N,j] - u[i,j]) * v[0] + S * (u[i,(j+1)%N] - u[i,j]) * v[1]
				ut[i,j] = (1 / V) * Q[i,j]
				u[i,j] = u[i,j] + dt * ut[i,j]
		surf = ax.plot_surface(X,Y,u)

	return surf

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ani = FuncAnimation(fig, compute_and_plot_solution, frames = range(steps), interval = dt * 1000)

plt.show()
