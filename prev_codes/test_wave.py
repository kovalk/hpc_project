from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from mpl_toolkits.mplot3d import Axes3D

N = 10

u = np.zeros((N,N))
u1 = np.zeros((N,N)) # = u_x
u2 = np.zeros((N,N)) # = u_y
u3 = np.zeros((N,N)) # = u_t (initial = 0)
Q1 = np.zeros((N,N))
Q2 = np.zeros((N,N))
Q3 = np.zeros((N,N))

t0 = 0.
t1 = 20.
dt = 0.005
steps = int( (t1 - t0) / dt ) 
S = 1 / N
V = 1 / (N * N)
csq = 1.0

X = np.zeros((N))
Y = np.zeros((N))
for i in range(N):
	X[i] = 0.5 * S + i * S
	Y[i] = 0.5 * S + i * S
X,Y = np.meshgrid(X,Y)
C = np.array([0.5,0.5])

def compute_and_plot_solution(step):

	ax.cla()

	if step == 0:
		# plot initial solution
		for i in range(N):
			for j in range(N):
				u[i,j] = np.exp(- 0.5 * (X[i,j]-C[0])**2 - 0.5 * (Y[i,j]-C[1])**2)
				u1[i,j] = - (X[i,j]-C[0]) * u[i,j]
				u2[i,j] = - (Y[i,j]-C[1]) * u[i,j] 
		surf = ax.plot_surface(X,Y,u3)
	else:
		# compute / plot solution at step > 0
		for i in range(N):
			for j in range(N):
				Q3[i,j] = 0
				if u1[i,j] >= 0:
					Q1[i,j] = u3[(i+1)%N,j] * S - u3[i,j] * S
					Q3[i,j] += csq * S * (u1[(i+1)%N,j] - u1[i,j])
				else:
					Q1[i,j] = u3[i,j] * S - u3[i-1,j] * S
					Q3[i,j] += csq * S * (u1[i,j] - u1[i-1,j])
				if u2[i,j] >= 0:
					Q2[i,j] = u3[i,(j+1)%N] * S - u3[i,j] * S
					Q3[i,j] += csq * S * (u2[i,(j+1)%N] - u2[i,j]) 
				else:
					Q2[i,j] = u3[i,j] * S - u3[i,j-1] * S	
					Q3[i,j] += csq * S * (u2[i,j] - u2[i,j-1])
		for i in range(N):
			for j in range(N):
				u1[i,j] = u1[i,j] + dt * (1 / V) * Q1[i,j]
				u2[i,j] = u2[i,j] + dt * (1 / V) * Q2[i,j]
				u3[i,j] = u3[i,j] + dt * (1 / V) * Q3[i,j] 
				u[i,j] = u[i,j] + dt * u3[i,j]
		surf = ax.plot_surface(X,Y,u3)

	return surf

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ani = FuncAnimation(fig, compute_and_plot_solution, frames = range(steps), interval = dt * 1000)

plt.show()
